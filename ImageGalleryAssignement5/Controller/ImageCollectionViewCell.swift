//
//  ImageCollectionViewCell.swift
//  ImageGalleryAssignement5
//
//  Created by Ionut Flocea on 08.03.2022.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Properties
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    static let identifier = "ImageCollectionViewCell"
    var cellImage: Image?
    
    var isLoading = true {
        didSet {
            if isLoading{
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    // MARK: - Life cycle
    
    override func prepareForReuse() {
      super.prepareForReuse()
      imageView.image = nil
      isLoading = true
    }
    
    func setCellImage(image: Image) {
        cellImage = image
        let data = try? Data(contentsOf: image.imagePath!)

        if let imageData = data {
            let image = UIImage(data: imageData)
            imageView.image = image
        }
        
            
    }
    

}


