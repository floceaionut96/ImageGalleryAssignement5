//
//  DisplayGalleryCollectionViewController.swift
//  ImageGalleryAssignement5
//
//  Created by Ionut Flocea on 08.03.2022.
//

import UIKit

class DisplayGalleryCollectionViewController: UICollectionViewController {
    
    //MARK: - Properties
    
    var currentGallery = ImageGallery(images: [], title: "")
    @IBOutlet var galleryCollectionView: UICollectionView!
    private var minimumImageWidth: CGFloat? {
        return collectionView.frame.size.width / 3
    }
    private var flowLayout: UICollectionViewFlowLayout? {
      return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    
    var imgFetchers: [ImageFetcher] = []
    
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        galleryCollectionView.delegate = self
        galleryCollectionView.dataSource = self
        flowLayout?.estimatedItemSize = .zero
      //  galleryCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
       //     .flowLayout.
        view.addInteraction(UIDropInteraction(delegate: self))
    }
    
    

}

    //MARK: - Delegate and DataSource

extension DisplayGalleryCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        print("TAP!!!!!")
    }
}

extension DisplayGalleryCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentGallery.images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath)
        if let imageCell = cell as? ImageCollectionViewCell {
            imageCell.setCellImage(image: currentGallery.images[indexPath.item])
        }
        return cell
    }
}

    //MARK: - Drop Delegate

extension DisplayGalleryCollectionViewController: UIDropInteractionDelegate {
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        
        let imageFetcher = ImageFetcher() { (url, image) in
            DispatchQueue.main.async {
                self.currentGallery.images.append(Image(imagePath: url, aspectRatio: 1.0))
                self.collectionView.reloadData()
                self.imgFetchers.removeAll()
            }
        }
        
        session.loadObjects(ofClass: NSURL.self) { nsurls in
            if let url = nsurls.first as? URL {
            imageFetcher.fetch(url)
            }
        }
        
        session.loadObjects(ofClass: UIImage.self) { images in
            if let image = images.first as? UIImage {
            imageFetcher.backup = image
            }
        }
        imgFetchers.append(imageFetcher)
        
    }
    
}
    //MARK: - FlowLayoutDelegate

extension DisplayGalleryCollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//      let galleryImage = gallery.images[indexPath.item]
//
//        let itemHeight = minimumImageWidth! / CGFloat(galleryImage.aspectRatio)

        return CGSize(width: 150, height: 150)
    }
    

}



