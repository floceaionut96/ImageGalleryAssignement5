//
//  ImageGallery.swift
//  ImageGalleryAssignement5
//
//  Created by Ionut Flocea on 08.03.2022.
//

import Foundation
import UIKit
struct ImageGallery {
    
    //MARK: - Properties
    var images: [Image]
    
    var title: String
    
    mutating func addElement(image: Image) {
        images.append(image)
    }
}


struct Image {
    
    //MARK: - Properties
    var imagePath: URL?
    var aspectRatio: Double
    
    //MARK: - Init
    
    init(imagePath: URL?, aspectRatio: Double){
        self.imagePath = imagePath
        self.aspectRatio = aspectRatio
    }
}
