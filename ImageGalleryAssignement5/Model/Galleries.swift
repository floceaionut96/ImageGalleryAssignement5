//
//  Galleries.swift
//  ImageGalleryAssignement5
//
//  Created by Ionut Flocea on 10.03.2022.
//

import Foundation

class Galleries {

     var galleries: [ImageGallery] = [ImageGallery(images: [], title: "My Gallery")]
    
     var currentGallery: ImageGallery
    
    init() {
        currentGallery = galleries[0]
    }
    
     func addNewGallery() {
        galleries.append(ImageGallery(images: [], title: "Untitled"))
    }
    
     func deleteGallery(index: Int) {
        galleries.remove(at: index)
    }
    
}
